Rails.logger.level = 0
require 'logger'
logger = ActiveSupport::Logger.new(STDOUT)
logger.info "============== Parsing user_auth_google.rb ================"


User.instance_eval do
  def self.find_for_google_oauth2(access_token, signed_in_resource=nil)
    logger.info "============================ #{access_token.inspect} ======================="

    email = access_token.info.email
    # Ares DB needs to query using user ID.
    # TODO: confirm that downcase'd usernames match in Ares
    username = email.split('@').first.downcase


    user = User.find_or_create_by_username_or_email(username, email)
    raise "Finding user (#{ user }) failed: #{ user.errors.full_messages }" unless user.persisted?

    user
  end
end
